#!/usr/bin/env ruby
require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'pg'
require 'dotenv'
require 'webrick'
Dotenv.load

require_relative 'db_client'

web_server = WEBrick::HTTPServer.new(Port: 3033)

# load assets
favicon  = IO.read('view/favicon.png')
main_css = IO.read('view/main.css')
ext_css  = IO.read('view/bootstrap.min.css')
ext_js   = IO.read('view/jquery.min.js')
ext_js  += IO.read('view/bootstrap.min.js')

web_server.mount_proc('/'){ |req, resp|
  case req.path
    when '/favicon.ico'
      resp['Content-Type'] = 'image/x-icon'
      resp.body = favicon
    when '/'
      b = binding
      all_data = DbClient.new.select_all
      b.local_variable_set(:all_data, all_data)
      resp['Content-Type'] = 'text/html'
      resp.body = ERB.new(IO.read('view/index.html.erb')).result(b)
  end
}
web_server.start











