#!/usr/bin/env ruby
require 'rubygems'
require 'pg'

class DbClient
  attr_reader :conn

  def initialize
    @conn = PG.connect dbname: ENV['DB_NAME'], user: ENV['DB_USER'], password: ENV['DB_PASS']
  rescue PG::Error => e
    puts e.message
  end

  def to_sql(data_array)
    formatted_sql = '(' + data_array.collect { |v| v.map(&:inspect).collect{|i| i.tr(",", ".")}.join(',') }.join('),(') + ')'
    formatted_sql.tr('"', "'").gsub('$', '').gsub("''", "'")
  end

  def select_all
    res = @conn.exec "SELECT * from #{ENV['DB_CURRENCIES_TABLE']}"
    # symbolize keys:
    res ? res.collect { |hash| hash.map { |k,v| [k.to_sym, v] }.to_h } : []
  end

  def update(columns, data)
    # Not a real 'update' function due to limited time set; I just truncates the table, before each update..
    @conn.exec %{TRUNCATE #{ENV['DB_CURRENCIES_TABLE']}}
    @conn.exec %{INSERT INTO #{ENV['DB_CURRENCIES_TABLE']} (#{columns}) VALUES #{to_sql(data)}}
  end
end
