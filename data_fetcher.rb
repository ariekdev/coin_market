#!/usr/bin/env ruby
require 'rubygems'
require 'nokogiri'
require 'open-uri'
require_relative 'db_client'

module DataFetcher
  class << self
    DATA_TABLE_SELECTORS = {
      rating:             ' tr td:first',
      name:               '.currency-name-container',
      code:               '.currency-symbol',
      price:              '.price',
      circulating_supply: '[data-supply-container]',
      market_cap:         ' table .market-cap',
      change:             '.percent-change[data-timespan="24h"]'
    }.freeze

    def data
      page = Nokogiri::HTML(open(ENV['DATA_SOURCE_URL']))
      DATA_TABLE_SELECTORS.values.map do |selector|
        page.css(selector).map(&:content).map(&:strip)
      end.transpose
    end

    def perform!
      DbClient.new.update(DATA_TABLE_SELECTORS.keys.join(','), data)
    end
  end
end