#!/usr/bin/env ruby
require 'rubygems'
require 'dotenv'
Dotenv.load

require_relative '../data_fetcher'
DataFetcher.perform!
