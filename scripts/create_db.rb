#!/usr/bin/env ruby
require 'rubygems'
require 'pg'
require 'dotenv'
Dotenv.load

begin
  p "establish PG connection (#{ENV['DB_NAME']} / #{ENV['DB_USER']} / #{ENV['DB_PASS']} ).."
  conn = PG.connect dbname: ENV['DB_NAME'], user: ENV['DB_USER'], password: ENV['DB_PASS']

  p "drop #{ENV['DB_CURRENCIES_TABLE']} table if exists.."
  conn.exec "DROP TABLE IF EXISTS #{ENV['DB_CURRENCIES_TABLE']}"

  p "create #{ENV['DB_CURRENCIES_TABLE']} table.."
  conn.exec %{
    CREATE TABLE #{ENV['DB_CURRENCIES_TABLE']}(
      id                  SERIAL NOT NULL PRIMARY KEY,
      rating              INT,
      name                VARCHAR(100),
      code                VARCHAR(20),
      price               VARCHAR(20),
      circulating_supply  VARCHAR(20),
      market_cap          VARCHAR(20),
      change              VARCHAR(20),
      created_at          TIMESTAMPTZ NOT NULL DEFAULT NOW())
    }
  p 'done.'

rescue PG::Error => e
  puts e.message
ensure
  conn.close if conn
  puts 'close PG connection.'
end