#!/usr/bin/env ruby
require_relative 'refresh_db'

REFRESH_TIME_SECONDS = 600

puts "Start auto refresh every #{REFRESH_TIME_SECONDS} seconds.."

# quick and dirty solution:
while true
  system("#{File.expand_path(File.dirname(__FILE__))}/refresh_db.rb")
  puts "data refreshed!"
  puts "sleep for next #{REFRESH_TIME_SECONDS} seconds.."
  sleep 10
end

# In real, I prefer to use Sidekiq for background processing. Moreover I implemented own handy gems, based on Sidekiq
