```
gem install nokogiri
gem install dotenv
gem install pg

```
...
#
######Service script:

    create_db.rb - drop/creates PG database, configured with env vars ('.env' file)
Usage:
    `$ ruby scripts/create_db.rb`
----------------
######Service script:

    auto_updater.rb - recurrent data refresh
Usage:
    `ruby ./scripts/auto_updater.rb`

#
###Web server:

    coin_server.rb - listens on 3033 port
     
Usage:
    `ruby coin_server.rb`

----------------

> There are 2 .env files in ./ and ./scripts paths.

#-__-_
_`vitalyp`_